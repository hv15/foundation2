############################### INFORMATION ###################################
#
# Created by Hans-Nikolai Viessmann (hv15 @ hw.ac.uk)
# Lasted Edited Date: 2013-03-13
#
# Foundations 2: 2013, Semester 2
# Lecturer: Joe B. Wells
# Assignment: Coursework, Part-3
# Further Information:
#      http://www.macs.hw.ac.uk/~jbw/teaching/hw/F29FB/2012,-13/assignment.html
# Due Date: by 17:00 on 2013-03-13
# 
#        File: main.py
#  Input file: simple-input.json
# Output file: output.txt
# 
# Classes:
#	mySet
#	myTuple
#
# Methods:
#      printer
#      outer
#      loadinput
#      badinput
#      variable
#      operator
#      startparse
#
"""
This part uses my previously made classes mySet and myTuple with a few extra
functions to deal with the testing for functions, inversion and so on.

First off, the input file (ONLY JSON formatted) is loaded and parsed json
object imported and used in the startparse(). At the same time the output file
is opened.

The actual expression parsing is then passed to the operator() and variable(),
they both work to gether to create new expression from either given values or
from other expressions stored in the 'parsed' list.

Depending on what the operation is, various methods will get called to create
the expressions. If at any point a expression can not be made, either
'undefined!' with be printed in the outfile, or 'BAD INPUT' will be written to
the outfile with an error printed to STDERR causing the application to exit.

The various reasons for this to happen include either syntax errors, or
missing valid expressions.
"""
import json
import sys
import os
from numbers import Number
from collections import Iterable

parsed = [] 		# create an empty list
outfile = None		# create variable for file pointer
	
def printer(*items):
	"""
	This method takes an arbitrary number of inputs and depending on their
	type writes them out to a file, or calls an object's output method. If
	an input does not match either str, int, MySet, or MyTuple, we ignore
	it and continue.

	param: *items -> arbitary number of inputs of any type
	"""
	for x in items:
		if isinstance(x, basestring) or isinstance(x, Number):
			outfile.write(str(x))
		elif isinstance(x, myTuple) or isinstance(x, mySet):
			x.output()

def outer(elements, lb, rb):
	"""
	This method is a wrapper for the printer method. For an input of
	elements, we printout the brackets, and then iterate through the
	elements passing them to the printer method.

	param: elements -> an object, such as the MySet or MyTuple objects
	param: lb -> is a character, such as a left bracket/brace, i.e '{',etc.
	param: rb -> like lb, but instead a right bracket/brace, i.e '}',etc.
	"""
	count = 1
	printer(lb)
	for x in elements:
		printer(x)
		if(count < len(elements)):
			printer(',')
		count += 1
	printer(rb)

def badinput(error):
	"""
	This method, when called, will write out the 'BAD INPUT' message to the
	file and will print an error message to STDERR

	param: error -> the error message to be printed
	"""
	outfile.write("BAD INPUT\n")
	outfile.close()
	sys.exit("%s Please look at 'output.txt'" % error)

class mySet(frozenset):
	"""
	This class encapsulates Python's frozenset object, thus inheriting
	the frozenset methods such as union(), intersection(), and difference().
	"""
	def __init__(self, obj):
		"""
		Initiates the MySet object, checks that the given input is a
		frozenset, if not we throw an Exception.

		param: obj -> the given object
		"""
		if not isinstance(obj, frozenset):
			"""
			Though perhaps a more informative error would be more
			useful, considering that this code is not dynamically
			taking in interactive arguments, the exception is to
			stop me from making an error.
			
			Can you guess where this quote is from?
			"""
			raise Exception("Ten thousand thundering typhoons!")
	def output(self):
		"""
		Method to call the outer() and pass this object to be
		printed.
		"""
		outer(self,'{','}')
	def range(self):
		"""
		Method to get the range of a relation and return it
		"""
		ranges = set()
		for item in self:
			if isinstance(item, myTuple):
				ranges.add(item[1])
			else:
				badinput("Not a relation!")
		return mySet(frozenset(ranges))
	def domain(self):
		"""
		Method to get the domain of a relation and return it
		"""
		domains = set()
		for item in self:
			if isinstance(item, myTuple):
				domains.add(item[0])
			else:
				badinput("Not a relation!")
		return mySet(frozenset(domains))	
	def isfunction(self):
		"""
		Method to test whether a relation is a function, specifically
		that each tuple's zeroth index (left-most value) is distinctly 
		unique in the relation
		"""
		seen = set()
		for item in self:
			if isinstance(item, myTuple) and item[0] not in seen:
				seen.add(item[0])
			else:
				return int(0)
		return int(1)
	def inverse(self):
		"""
		Method to invert a relation
		"""
		inverse = set()
		for item in self:
			if isinstance(item, myTuple) and len(item) == 2:
				inverse.add(item.invert())
			else:
				badinput("Relation not invertable!")
		return mySet(frozenset(inverse))
	def isinjective(self):
		"""
		Method to test whether a relation is injective, that is to say
		that each tuple's zeroth index (left-most value) is distinctly
		unique and that it relates distinctly to a unique output (the
		tuple's right-most value)
		"""
		left = set()
		right = set()
		for item in self:
			if isinstance(item, myTuple) and len(item) == 2:
				if item[0] not in left and item[1] not in right:
					left.add(item[0])
					right.add(item[1])
				else:
					return int(0)
			else:
				return int(0)
		return int(1)


class myTuple(tuple):
	"""
	Like the MySet class, we encapsulate Python's tuple object.
	"""
	def __init__(self, obj):
		"""
		Initiates the MyTuple object, checks that the given input is a
		tuple, if not we throw an Exception.

		Furthermore, as part of the 'Fun extension', we check the
		number of elements held within the tuple, ensureing that for k
		number of elements, k = 0 or k >= 2.
		
		If not, we throw an Exception.

		param: iterable -> the given object
		"""
		if not isinstance(obj, tuple):
			"""
			See the comment above in MySet

			Can you guess this one?
			I'll give you a hint: John Hammond
			"""
			raise Exception("Ah ah ah, you didn't say the magic word!")
		if len(obj) == 1:
			"""
			See the comment above in MySet
			
			Now this one should be obvious.
			"""
			raise Exception("Houston, we have a problem...")
	def output(self):
		"""
		Method to call the outer() and pass this object to be
		printed.
		"""
		outer(self,'(',')')
	def invert(self):
		"""
		Method to invert a tuple and return the inverted tuple
		"""
		return myTuple(self[::-1])

def loadinput():
	"""
	A method to ensure that an argument is given. The given input is
	checked to ensure that the path is vaild. Only then is the file passed
	on.
	"""
	if len(sys.argv) < 2:
		sys.exit("Usage: %s input.json" % sys.argv[0])
	elif not os.path.exists(sys.argv[1]):
		sys.exit("ERROR: file %s not found!" % sys.argv[1])
	else:
		return sys.argv[1]

def variable(args):
	"""
	The is methods checks a givin input for whether it is a instance of
	Number, or is a dict containing the indexs 'variable' or 'operator'.

	Respectively, it either returns the input, gets the a value from a
	list, or calls the operator().

	param: args -> a varaible of arbitrary type
	"""
	if isinstance(args, Number):
		return args
	elif "variable" in args:
		return next((item[1]
			for item in parsed
			if item[0] == args["variable"]), "undefined!")
	elif "operator" in args:
		return operator(args)

def operator(objs):
	"""
	For a given object of type dict, check for its operator type. Depended
	upon this, either write out is values, create myTuple or mySet, or test
	for equility or memebership within set or tuple.

	param: objs -> dict containing an index 'operator' and an index
                       'aurguments'
	"""
	if objs["operator"] == "assign":
		# this here is the 'super' operator, this how variabe
		# expression pairs are made
		parsed.append([objs["arguments"][0]["variable"],
			variable(objs["arguments"][1])])
		printer(objs["arguments"][0]["variable"]," = ",
				variable(objs["arguments"][1]),";","\n")
	elif objs["operator"] == "set":
		# create a mySet object 
		s = map(variable, objs["arguments"])
		if "undefined!" in s:
			return "undefined!"
		else:
			return mySet(frozenset(s))
	elif objs["operator"] == "tuple":
		# create a myTuple object
		s = map(variable, objs["arguments"])
		if "undefined!" in s:
			return "undefined!"
		else:
			return myTuple(tuple(s))
	elif objs["operator"] == "equal":
		# check for that both arguments are equal (type and contents) 
		var1 = variable(objs["arguments"][0])
		var2 = variable(objs["arguments"][1])
		return int(isinstance(var1, type(var2)) and var1 == var2)
	elif objs["operator"] == "member":
		# test whether an argument is in the other argument
		# ensuring that the right argument is iterable
		var1 = variable(objs["arguments"][0])
		var2 = variable(objs["arguments"][1])
		return int(isinstance(var2, Iterable) and var1 in var2)
	elif objs["operator"] == "is-function":
		# test whether the argument is a function
		# meaning that each memeber of the set and a left-most value
		# that is distinctly unique through the set
		var = variable(objs["arguments"][0])
		if isinstance(var, mySet):
			return var.isfunction()
		else:
			return int(0)
	elif objs["operator"] == "is-injective":
		# test whether the argument is injective
		# meaning that for each element of the set, its left-most value
		# is distinctly unique throughout the set with the its
		# right-most value distinctly related to it throughout the set
		var = variable(objs["arguments"][0])
		if isinstance(var, mySet):
			return var.isinjective()
		else:
			return int(0)
	elif objs["operator"] == "apply-function":
		# apply a value to function
		# meaning to look for a index within the members of the set,
		# and return its related value if found.
		var1 = variable(objs["arguments"][0])
		var2 = variable(objs["arguments"][1])
		found = []
		if isinstance(var1, myTuple):
			if var1[0] == var2:
				found.append(var1)
		elif isinstance(var1, mySet):
			for pair in var1:
				if isinstance(pair, myTuple):
					if pair[0] == var2:
						found.append(pair)
		if len(found) == 1:
			return found[0][1]
		else:
			return "undefined!"
	elif objs["operator"] == "union":
		var1 = variable(objs["arguments"][0])
		var2 = variable(objs["arguments"][1])
		if isinstance(var1, mySet) and isinstance(var2, mySet):
			return var1.union(var2)
		else:
			badinput("Not a set, can not do union!")
	elif objs["operator"] == "intersection":
		var1 = variable(objs["arguments"][0])
		var2 = variable(objs["arguments"][1])
		if isinstance(var1, mySet) and isinstance(var2, mySet):
			return var1.intersection(var2)
		else:
			badinput("Not a set, can not do intersection!")
	elif objs["operator"] == "set-difference":
		var1 = variable(objs["arguments"][0])
		var2 = variable(objs["arguments"][1])
		if isinstance(var1, mySet) and isinstance(var2, mySet):
			return var1.difference(var2)
		else:
			badinput("Not a set, can not do difference!")
	elif objs["operator"] == "domain":
		var = variable(objs["arguments"][0])
		if isinstance(var, mySet):
			return var.domain()
		else:
			badinput("Not a set, can not do domain!")
	elif objs["operator"] == "range":
		var = variable(objs["arguments"][0])
		if isinstance(var, mySet):
			return var.range()
		else:
			badinput("Not a set, can not do range!")
	elif objs["operator"] == "inverse":
		# invert a tuple or the members of a set
		var = variable(objs["arguments"][0])
		if isinstance(var, mySet):
			return var.inverse()
		elif isinstance(var, myTuple):
			return var.invert()
		else:
			badinput("Not valid structures, can not invert!")
	else:
		badinput("Invalid operator detected! Please see 'output.txt'")

def startparse():
	"""
	This method is the initiator, which runs through all of the functions,
	including the loading of the input file, the parsing of the input,
	and writing of the output file.
	"""
	global outfile
	
	jsonfile = open(loadinput(), "r")
	outfile = open("output.txt", "w")

	jsontext = json.load(jsonfile)
	jsonfile.close()

	# ensure that we are dealing with individual arrays
	jsontext = jsontext["statement-list"]
	
	for i in jsontext:
		# This is a bit of a hack to ensure that there is a difference
		# between 'equal' at the first level and subsiquent 'equal'
		if(i["operator"] == "equal"):
			i["operator"] = "assign"
		# begin the parsing
		operator(i)

	outfile.close()

# This is where we begin :) finally...
startparse()
