#!/usr/bin/env python
# -*- coding: utf8 -*-
############################### INFORMATION ###################################
#
# Created by Hans-Nikolai Viessmann (hv15 @ hw.ac.uk)
# Lasted Edited Date: 2013-01-27
#
# Foundations 2: 2013, Semester 2
# Lecturer: Joe B. Wells
# Assignment: Coursework, Part-1
# Further Information:
#      http://www.macs.hw.ac.uk/~jbw/teaching/hw/F29FB/2012,-13/assignment.html
# Due Date: by 17:00 on 2013-01-27
# 
# File: main.py
# 
# Classes:
#      MySet
#      MyTuple
#
# Functions:
#      printer
#      outer
#
############################### DOCUMENTATION #################################
# 
# I have created an array and appended to it the various sets and pairs as
# given in the assignment. After this is done, I loop over each element in the
# array and pass it to the printer().
#
# If a given input is of type string or integer, we write it to STDOUT. If not,
# we check to see if a given input is of type MySet or MyTuple. If this is the
# case, we call the object's output(), otherwise we ignore the input and
# continue.
#
# The output() simply calls the outer() and passes the object and appropriate
# brackets ('{' '}' for sets, '(' ')' for pairs). In the outer(), we print out
# the left backet, and then iterate over the elements held within the object (if
# any). Using a counter, we print out a comma (',') after each element, except
# the last one. Lastly we print out the right bracket. The outer() uses the
# printer() to do all the printing.
#
# I have chosen to implement my own classes to encapsulate Python's builtin
# frozenset and tuple. The reason for this was to create a method (output())
# that could be used to recursively loop across all elements of a given object.
#
# In the case of MyTuple, I have furthermore implemented, as part of the 'Fun 
# extension', a check to ensure that the tuple can never be a singleton.
# Examples of this can be viewed at the end of this file, they have been
# commented out to not interact with the actual assignment.
#
###############################################################################
import sys

def printer(*input):
	"""
	This method takes an arbitrary number of inputs and depending on their type
	writes them out to STDOUT, or calls an object's output method. If an input
	does not match either str, int, MySet, or MyTuple, we ignore it and
	continue.

	param: *input -> arbitary number of inputs of any type
	"""
	for x in input:
		if(type(x) in [str, int]):
			sys.stdout.write(str(x))
		elif(type(x) in [MySet, MyTuple]):
			x.output()

def outer(elements, lb, rb):
	"""
	This method is a wrapper for the printer method. For an input of
	elements, we printout the brackets, and then iterate through the elements
	passing them to the printer method.

	param: elements -> an object, such as the MySet or MyTuple objects
	param: lb -> is a character, such as a left bracket/brace, i.e '{', etc.
	param: rb -> like lb, but instead a right bracket/brace, i.e '}', etc.
	"""
	count = 1
	printer(lb)
	for x in elements:
		printer(x)
		if(count < len(elements)):
			printer(',')
		count += 1
	printer(rb)

class MySet(frozenset):
	"""
	This class encapsulates Python's frozenset object, thus inheriting
	the frozenset methods such as union(), intersection(), and difference().
	"""
	def __init__(self, iterable):
		"""
		Initiates the MySet object, checks that the given input is a
		frozenset, if not we throw an Exception.

		param: iterable -> the given object
		"""
		if(type(iterable) is not frozenset):
			# Though perhaps a more informative error would be more
			# useful, considering that this code is not dynamically
			# taking in interactive arguments, the exception is to
			# stop me from making an error.
			#
			# Can you guess where this quote is from?
			raise Exception("Ten thousand thundering typhoons!")
	def output(self):
		"""
		Function to call the outer() and pass this object to be printed.
		"""
		outer(self,'{','}')

class MyTuple(tuple):
	"""
	Like the MySet class, we encapsulate Python's tuple object.
	"""
	def __init__(self, iterable):
		"""
		Initiates the MyTuple object, checks that the given input is a
		tuple, if not we throw an Exception.

		Furthermore, as part of the 'Fun extension', we check the number of
		elements held within the tuple, ensureing that for k number of
		elements, k = 0 or k >= 2. If not, we throw an Exception.

		param: iterable -> the given object
		"""
		if(type(iterable) is not tuple):
			# See the comment above in MySet
			#
			# Can you guess this one? I'll give you a hint: John Hammond
			raise Exception("Ah ah ah, you didn't say the magic word!")
		if(len(iterable) == 1):
			# See the comment above in MySet
			#
			# Now this one should be obvious.
			raise Exception("Houston, we have a problem...")
	def output(self):
		"""
		Function to call the outer() and pass this object to be printed.
		"""
		outer(self,'(',')')

x = [] # the empty array, hurray!
# x₀ = 8
x.append(8)
# x₁ = {1,2,3,4,5,6,7,x₀}
x.append(MySet(frozenset([1,2,3,4,5,6,7,x[0]])))
# x₂ = {x₁,(1,x₁)}
x.append(MySet(frozenset([x[1],MyTuple(tuple([1,x[1]]))])))
# x₃ = (x₂,x₁)
x.append(MyTuple(tuple([x[2],x[1]])))
# x₄ = {x₃} ∪ x₂
x.append(MySet(frozenset([x[3]])).union(x[2]))
# x₅ = x₄ ∖ {x₁}
x.append(x[4].difference(MySet(frozenset([x[1]]))))
# x₆ = x₄ ∩ {x₁}
x.append(x[4].intersection(MySet(frozenset([x[1]]))))

######## FUN EXTENSION ########
# x.append(MyTuple(tuple())) # will output '();'
# x.append(MyTuple(tuple([1,2,3,4]))) # will output '(1,2,3,4);'
# x.append(MyTuple(tuple([1]))) # will throw an Exception!
###############################

# This is where the magic happens ;-)
for out in range(len(x)):
	# Loop over the elements held within the array
	# and for each one call the printer().
	printer('x',out,' = ',x[out],';','\n')
	# Due to the fact that I am using Python's sys.stdout.write(),
	# I need to pass the newline character, else everything would
	# be on one line, efficent perhaps, but hardly readable.
