
############################### INFORMATION ###################################
#
# Created by Hans-Nikolai Viessmann (hv15 @ hw.ac.uk)
# Lasted Edited Date: 2013-02-26
#
# Foundations 2: 2013, Semester 2
# Lecturer: Joe B. Wells
# Assignment: Coursework, Part-2
# Further Information:
#      http://www.macs.hw.ac.uk/~jbw/teaching/hw/F29FB/2012,-13/assignment.html
# Due Date: by 17:00 on 2013-02-26
# 
# File: main.py
# Input file: input.json
# Output file: output.txt
# 
# Classes:
#      MyTuple
#
# Methods:
#      printer
#      outer
#      loadinput
#      variable
#      operator
#      startparse
#
############################### DOCUMENTATION #################################
# This part uses my previously made classes mySet and myTuple.
#
# This part takes in an input file (ONLY JSON formatted) and parse it,
# interpreting the formatted sets notation to thus generate
# "VARIABLE = EXPRESSION;" one line at a type.
#
###############################################################################
import json
import sys
import os
from numbers import Number
from collections import Iterable

x = [] # create an empty list
outfile = None
	
def printer(*items):
	"""
	This method takes an arbitrary number of inputs and depending on their
	type writes them out to a file, or calls an object's output method. If
	an input does not match either str, int, MySet, or MyTuple, we ignore
	it and continue.

	param: *input -> arbitary number of inputs of any type
	"""
	for x in items:
		if isinstance(x, (basestring, Number)):
			outfile.write(str(x))
		elif(type(x) in [mySet, myTuple]):
			x.output()

def outer(elements, lb, rb):
	"""
	This method is a wrapper for the printer method. For an input of
	elements, we printout the brackets, and then iterate through the
	elements passing them to the printer method.

	param: elements -> an object, such as the MySet or MyTuple objects
	param: lb -> is a character, such as a left bracket/brace, i.e '{',etc.
	param: rb -> like lb, but instead a right bracket/brace, i.e '}',etc.
	"""
	count = 1
	printer(lb)
	for x in elements:
		printer(x)
		if(count < len(elements)):
			printer(',')
		count += 1
	printer(rb)

class mySet(frozenset):
	"""
	This class encapsulates Python's frozenset object, thus inheriting
	the frozenset methods such as union(), intersection(), and difference().
	"""
	def __init__(self, obj):
		"""
		Initiates the MySet object, checks that the given input is a
		frozenset, if not we throw an Exception.

		param: obj -> the given object
		"""
		if(type(obj) is not frozenset):
			"""
			Though perhaps a more informative error would be more
			useful, considering that this code is not dynamically
			taking in interactive arguments, the exception is to
			stop me from making an error.
			
			Can you guess where this quote is from?
			"""
			raise Exception("Ten thousand thundering typhoons!")
	def output(self):
		"""
		Function to call the outer() and pass this object to be
		printed.
		"""
		outer(self,'{','}')

class myTuple(tuple):
	"""
	Like the MySet class, we encapsulate Python's tuple object.
	"""
	def __init__(self, obj):
		"""
		Initiates the MyTuple object, checks that the given input is a
		tuple, if not we throw an Exception.

		Furthermore, as part of the 'Fun extension', we check the
		number of elements held within the tuple, ensureing that for k
		number of elements, k = 0 or k >= 2.
		
		If not, we throw an Exception.

		param: iterable -> the given object
		"""
		if(type(obj) is not tuple):
			"""
			See the comment above in MySet

			Can you guess this one?
			I'll give you a hint: John Hammond
			"""
			raise Exception("Ah ah ah, you didn't say the magic word!")
		if(len(obj) == 1):
			"""
			See the comment above in MySet
			
			Now this one should be obvious.
			"""
			raise Exception("Houston, we have a problem...")
	def output(self):
		"""
		Function to call the outer() and pass this object to be
		printed.
		"""
		outer(self,'(',')')

def loadinput():
	"""
	A method to ensure that an argument is given. The given input is
	checked to ensure that the path is vaild. Only then is the file passed
	on.
	"""
	if len(sys.argv) < 2:
		sys.exit("Usage: %s input.json" % sys.argv[0])
	elif not os.path.exists(sys.argv[1]):
		sys.exit("ERROR: file %s not found!" % sys.argv[1])
	else:
		return sys.argv[1]

def variable(args):
	"""
	The is methods checks a givin input for whether it is a instance of
	Number, or is a dict containing the indexs 'variable' or 'operator'.

	Respectively, it either returns the input, gets the a value from a
	list, or calls the operator().

	param: args -> a varaible of arbitrary type
	"""
	if isinstance(args, Number):
		return args
	elif "variable" in args:
		return next((item[1] for item in x if item[0] == args["variable"]), "undefined!")
	elif "operator" in args:
		return operator(args)

def operator(objs):
	"""
	For a given object of type dict, check for its operator type. Depended
	upon this, either write out is values, create myTuple or mySet, or test
	for equility or memebership within set or tuple.

	param: objs -> dict containing an index 'operator' and an index
                       'aurguments'
	"""
	if objs["operator"] == "assign":
		x.append([objs["arguments"][0]["variable"],
			variable(objs["arguments"][1])])
		printer(objs["arguments"][0]["variable"]," = ",variable(objs["arguments"][1]),";","\n")
	elif objs["operator"] == "set":
		s = map(variable, objs["arguments"])
		if "undefined!" in s:
			return "undefined!"
		else:
			try:
				return mySet(frozenset(s))
			except:
				printer("BAD INPUT","\n")
				sys.exit("Invalid datatype detected! Please see 'output.txt'")
	elif objs["operator"] == "tuple":
		s = map(variable, objs["arguments"])
		if "undefined!" in s:
			return "undefined!"
		else:
			try:
				return myTuple(tuple(s))
			except:
				printer("BAD INPUT","\n")
				sys.exit("Invalid datatype detected! Please see 'output.txt'")
	elif objs["operator"] == "equal":
		var1 = variable(objs["arguments"][0])
		var2 = variable(objs["arguments"][1])
		return int(type(var1) == type(var2) and var1 == var2)
	elif objs["operator"] == "member":
		var1 = variable(objs["arguments"][0])
		var2 = variable(objs["arguments"][1])
		return int(isinstance(var2, Iterable) and var1 in var2)

def startparse():
	"""
	This method is the initiator, which runs through all of the functions,
	including the loading of the input file, the parsing of the input,
	and writing of the output file.
	"""
	global outfile
	
	jsonfile = open(loadinput(), "r")
	outfile = open("output.txt", "w")

	jsontext = json.load(jsonfile)
	jsonfile.close()

	# ensure that we are dealing with individual arrays
	jsontext = jsontext["statement-list"]
	
	for i in jsontext:
		# This is a bit of a hack to ensure that there is a difference
		# between 'equal' at the first level and subsiquent 'equal'
		if(i["operator"] == "equal"):
			i["operator"] = "assign"
		# begin the parsing
		operator(i)

	outfile.close()

# This is where we begin :) finally...
startparse()
